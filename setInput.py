# Several function loading the stimulus from any BMP into the simulation of LaminartWithSegmentation*.py
# coding: utf-8


######################################################################################
### SEGMENTATION SIGNAL LOCATIONS GO IN : def chooseSegmentationSignal (see below) ###
######################################################################################


# Imports:
import numpy, random, os
from PIL import Image


# Function to read any BMP file as an image and transform it into a numpy array ; crops the white background
def readBMP(thisConditionName, onlyZerosAndOnes=0):

    # Read the image and transforms it into a [0 to 254] array
    dirPath = os.getcwd() + '/Stimuli/'
    im = Image.open(dirPath+thisConditionName+'.bmp')
    im = numpy.array(im)
    if len(numpy.shape(im)) > 2:                # if the pixels are [r,g,b] and not simple integers
        im = numpy.mean(im,2)                   # mean each pixel to integer (r+g+b)/3
    if onlyZerosAndOnes == 1:
        im = numpy.round(im/im.max())*im.max()  # either 0 or im.max()
    if onlyZerosAndOnes == 2:
        im = 0.5*(im + numpy.round(im/im.max())*im.max())  # less aliasing ...
    im *= 254.0/im.max()                        # array of floats between 0.0 and 254.0 (more useful for the Laminart script)
    white = im.max()                            # to detect the parts to crop

    # Send the image to the program
    return im, im.shape[0], im.shape[1]


# Find the vernier location into pixels, and create a template to compute a template match score
def findVernierAndCreateTemplates(pixels, thisConditionName, templateSize=None):

    # Vernier parameters (change according to stimulus) ; usual = 1.0, 0, 10, 1, 1, 2*vBarLength-2
    vSide        = 1.0  # 1.0 for bottom bar on the right, -1.0 for left ; here is the default one (do not change)
    vBarSpace    = 0    # least vertical space between vernier bars ; in pixels
    vBarLength   = 10   # length of one vernier bar ; best as an even number ; in pixels
    vBarWidth    = 1    # width of each vernier bars ; in pixels
    vBarShift    = 1    # horizontal space between vernier bars ; in pixels
    if templateSize == None:
        templateSize = 2*vBarLength-2  # size of the template (side length) ; in pixels ; 2*vBarLength-2 usually

    # V4 Brightness template for a vernier (left or right is for bottom line)
    ImageNumPixelRows, ImageNumPixelColumns = numpy.shape(pixels)
    RightVernierTemplate = numpy.zeros((ImageNumPixelRows, ImageNumPixelColumns))
    LeftVernierTemplate  = numpy.zeros((ImageNumPixelRows, ImageNumPixelColumns))

    # Randomly choose the vernier side (1/2 chance for each side)
    vSide = numpy.int(vSide/2 + 0.5)  # 1 for right bottom bar, 0 for left (from 1 to 1 and -1 to 0)

    # Create the corresponding vernier filter
    [Vx, Vy] = [2*vBarLength, 2*vBarWidth + vBarShift]  # usually [2*10,3]
    vernierFilter = -numpy.ones([Vx, Vy])
    vernierFilter[0         :1*vBarLength, 0*vSide + 2*(1-vSide)] = 1.0
    vernierFilter[vBarLength:2*vBarLength, 2*vSide + 0*(1-vSide)] = 1.0

    # Check for the Vernier location on the stimulus
    matchMax = 0
    [vernierLocX, vernierLocY] = [0, 0]  # location of the top-left corner of the vernier on the stimulus
    for i in range(ImageNumPixelRows - Vx + 1):
        for j in range(ImageNumPixelColumns - Vy + 1):
            match = numpy.sum(vernierFilter * pixels[i:i + Vx, j:j + Vy])
            if match > matchMax:
                matchMax = match
                [vernierLocY, vernierLocX] = [i, j]  # LocX corresponds to a column and LocY to a row

    # Bottom right
    firstRow    = max(vernierLocY + vBarLength  , 0)
    lastRow     = min(firstRow    + templateSize, ImageNumPixelRows)
    firstColumn = max(vernierLocX + 2           , 0)
    lastColumn  = min(firstColumn + templateSize, ImageNumPixelColumns)
    RightVernierTemplate[firstRow:lastRow, firstColumn:lastColumn] = 1.0

    # Top left
    lastRow     = min(vernierLocY + vBarLength  , ImageNumPixelRows)
    firstRow    = max(lastRow     - templateSize, 0)
    lastColumn  = min(vernierLocX + 1           , ImageNumPixelColumns)
    firstColumn = max(lastColumn  - templateSize, 0)
    RightVernierTemplate[firstRow:lastRow, firstColumn:lastColumn] = 1.0

    # Bottom left
    firstRow    = max(vernierLocY + vBarLength  , 0)
    lastRow     = min(firstRow    + templateSize, ImageNumPixelRows)
    lastColumn  = min(vernierLocX + 1           , ImageNumPixelColumns)
    firstColumn = max(lastColumn  - templateSize, 0)
    LeftVernierTemplate[firstRow:lastRow, firstColumn:lastColumn] = 1.0

    # Top right
    lastRow     = min(vernierLocY + vBarLength  , ImageNumPixelRows)
    firstRow    = max(lastRow     - templateSize, 0)
    firstColumn = max(vernierLocX + 2           , 0)
    lastColumn  = min(firstColumn + templateSize, ImageNumPixelColumns)
    LeftVernierTemplate[firstRow:lastRow, firstColumn:lastColumn] = 1.0

    # Return the position of the vernier and the created template
    vernierLocX += vBarWidth  + numpy.int((vBarShift-1)/2)
    vernierLocY += vBarLength + numpy.int((vBarSpace-1)/2)
    if thisConditionName in ['VdB1', 'VdB2', 'FGAssign', 'FGAssign2', 'FGAssign3', 'Harrisson', 'filling in 1', 'filling in 2', 'filling in 4']:
        [vernierLocY, vernierLocX] = [int(ImageNumPixelRows/2), int(ImageNumPixelColumns/2)]
    return vernierLocX, vernierLocY, RightVernierTemplate, LeftVernierTemplate


# Choose the optimal segmentation signal locations and stimulus type (which type of segmentation signal to use)
def chooseSegmentationSignal(thisConditionName, numSegLayers):

    ######################
    ### Default values ###
    ######################

    # One segmentation signal on the right, one on the left (25 pixels from target)
    segLocations = [0]*4  # (2*(numSegLayers-1))
    segLocations[0] =  25
    segLocations[2] = -25

    #######################################################
    ### Specific values for symmetric flanking patterns ###
    #######################################################

    # Just vernier (attracts segmentation)
    if thisConditionName in ['vernier']:
        segLocations[0:4] = [0, 0, 0, 0]
    # Only 1 flanking shape around vernier
    if thisConditionName in ['squares 1', 'squares test', 'squares 1_flankers', 'squares 1mod', 'circles 2', 'patterns2 2',
                             'stars 2', 'stars 2b', 'stars 6', 'pattern stars 2', 'hexagons 2', 'hexagons 7', 'octagons 2',
                             'octagons 7', 'irreg1 2', 'irreg2 2', 'pattern irregular 2']:
        segLocations[0:4] = [8, 0, -8, 0]
    # Flanking shape around vernier + 1 or more on each side
    if thisConditionName in ['cuboids', 'scrambled cuboids', 'squares 2', 'squares einat 2',
                             'circles 3', 'circles 4', 'circles 5', 'circles 6', 'circles test', 'triangles 2', 'triangles 3',
                             'pattern2 3', 'pattern2 4', 'pattern2 5', 'pattern2 6', 'pattern2 7', 'pattern2 8', 'pattern2 9', 'pattern2 10',
                             'stars 3', 'stars 4', 'stars 5', 'stars 5b', 'stars 7', 'stars 8', 'stars 9',
                             'pattern stars 3', 'pattern stars 5', 'pattern stars 6', 'pattern stars 7', 'pattern stars 8',
                             'pattern stars 9', 'pattern stars 10', 'pattern stars 11', 'pattern stars 13', 'pattern stars 14',
                             'hexagons 3', 'hexagons 4', 'hexagons 5', 'hexagons 6', 'hexagons 8', 'hexagons 9', 'hexagons 10', 'hexagons 11',
                             'octagons 3', 'octagons test', 'octagons 4', 'octagons 5', 'octagons 6', 'octagons 8', 'octagons 9', 'octagons 10', 'octagons 11', 'octagons grouping',
                             'irreg1 3', 'irreg1 4', 'irreg1 5', 'irreg1 6', 'irreg1 7', 'irreg1 8', 'irreg1 9',
                             'irreg1 10', 'irreg2 3', 'irreg2 4', 'irreg2 5', 'irreg2 identical',
                             'pattern irregular 3', 'pattern irregular 4', 'pattern irregular 5', 'pattern irregular 6', 'pattern irregular 7',
                             'pattern irregular 8', 'pattern irregular 9', 'pattern irregular 10', 'pattern irregular 11']:
        segLocations[0:4] = [34, 0, -34, 0]
    # Five squares around vernier
    if thisConditionName in ['squares 3']:
        segLocations[0:4] = [40, 0, -40, 0]
    # 7 squares around vernier
    if thisConditionName in ['squares 4', 'pattern stars 4', 'pattern stars 4b']:
        segLocations[0:4] = [50, 0, -50, 0]
    # Same as previous condition, but vertically shaped
    if thisConditionName == 'pattern stars 12':
        segLocations[0:4] = [0, 40, 0, -40]
    # Box or barred cross at both sides of the vernier
    if thisConditionName in ['boxes', 'boxes flankers', 'crosses']:
        segLocations[0:4] = [19, 0, -19, 0]
    # Box with a cross within at both sides of the vernier
    if thisConditionName == 'boxes and crosses':
        segLocations[0:4] = [14, 0, -14, 0]
    # One flanking line
    if thisConditionName in ['malania short 1', 'malania short 1 08', 'malania short 1 18', 'malania short 1 39', 'malania equal 1', 'malania long 1']:
        segLocations[0:4] = [5, 0, -5, 0]
    # Two flanking lines
    if thisConditionName in ['malania short 2', 'malania equal 2', 'malania long 2']:
        segLocations[0:4] = [10, 0, -10, 0]
    # More than two flanking lines
    if thisConditionName in ['malania short 4', 'malania short 8', 'malania short 16',
                             'malania equal 4', 'malania equal 8', 'malania equal 16',
                             'malania long 4', 'malania long 8', 'malania long 16']:
        segLocations[0:4] = [20, 0, -20, 0]
    # Stimuli suggesting foreground and background shapes
    if thisConditionName in ['foreground background 1', 'foreground background 2']:
        segLocations[0:4] = [0, 18, 0, 18]
    # Stimuli about shape similarity vs illusory contours (good continuation)
    if thisConditionName in ['squares_0_0']:
        segLocations[0:4] = [30, 0, -30, 0]
    if thisConditionName in ['squares_0_22', 'squares_0_45']:
        segLocations[0:4] = [35, 0, -35, 0]
    if thisConditionName in ['squares_22_0', 'squares_22_22', 'squares_22_45']:
        segLocations[0:4] = [30, 15, -30, -15]
    if thisConditionName in ['squares_45_0', 'squares_45_22', 'squares_45_45']:
        segLocations[0:4] = [30, 30, -30, -30]
    # Stimulus for fast spreading test
    if thisConditionName in ['boxes long']:
        segLocations[0:4] = [60, 0, -60, 0]
    # Stimulus for filling in test
    if thisConditionName in ['filling in 1', 'filling in 2']:
        segLocations[0:4] = [30, 0, -30, 0]
    # Stimulus from Erik Van der Burg paradigms
    if thisConditionName in ['VdB1', 'VdB2']:
        segLocations[0:4] = [60, 0, -60, 0]


    ########################################################
    ### Specific values for asymmetric flanking patterns ###
    ########################################################

    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1R']:
        segLocations[0:4] = [5, 0, -1000, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1L']:
        segLocations[0:4] = [1000, 0, -5, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1Rb']:
        segLocations[0:4] = [9, 0, -1000, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1Lb']:
        segLocations[0:4] = [1000, 0, -9, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1Rc']:
        segLocations[0:4] = [17, 0, -1000, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1Lc']:
        segLocations[0:4] = [1000, 0, -17, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1Rd']:
        segLocations[0:4] = [33, 0, -1000, 0]
    # Stimuli from Manassi's paper
    if thisConditionName in ['asym 1Ld']:
        segLocations[0:4] = [1000, 0, -33, 0]
    # One square on the left, 8 long bars on the right
    if thisConditionName in ['asym 2R']:
        segLocations[0:4] = [24, 0, -16, 0]
    # One square on the right, 8 long bars on the left
    if thisConditionName in ['asym 2L']:
        segLocations[0:4] = [16, 0, -24, 0]
    # One square on the left or right
    if thisConditionName in ['asym 3R']:
        segLocations[0:4] = [14, 0, -1000, 0]
    # One square on the left or right
    if thisConditionName in ['asym 3L']:
        segLocations[0:4] = [1000, 0, -14, 0]
    # One square around Vernier and 1 square on the right
    if thisConditionName in ['asym 4R']:
        segLocations[0:4] = [25, 0, -1000, 0]
    # One square around Vernier and 1 square on the left
    if thisConditionName in ['asym 4L']:
        segLocations[0:4] = [1000, 0, -25, 0]
    # One square around Vernier and 2 square on the right
    if thisConditionName in ['asym 5R']:
        segLocations[0:4] = [42, 0, -1000, 0]
    # One square around Vernier and 2 square on the left
    if thisConditionName in ['asym 5L']:
        segLocations[0:4] = [1000, 0, -42, 0]
    # One square around Vernier and 3 square on the right
    if thisConditionName in ['asym 6R']:
        segLocations[0:4] = [60, 0, -1000, 0]
    # One square around Vernier and 3 square on the left
    if thisConditionName in ['asym 6L']:
        segLocations[0:4] = [1000, 0, -60, 0]


    ##############################################
    ###  Return the locations to the computer  ###
    ##############################################

    return segLocations[0:2*(numSegLayers-1)]


# Create a distorted segmentation signal on the visual space from a top-down signal on the cortical representation
def createSegMap(useCMF, srcPixels, name, segLocX, segLocY, segSize, segSD, ecc, pixPerDeg, a, b):

    # Vernier parameters
    eccInPix = ecc*pixPerDeg  # distance from fixation point (fovea) to vernier, in pixels
    vernierLocX, vernierLocY, RightVernierTemplate, LeftVernierTemplate = findVernierAndCreateTemplates(srcPixels, name)

    # Load input size and parameters
    imageNumPixelRows = srcPixels.shape[0]
    imageNumPixelCols = srcPixels.shape[1]
    leftCenterPixCol  = eccInPix - vernierLocX
    rightCenterPixCol = eccInPix - vernierLocX + imageNumPixelCols
    topCenterPixRow   = 0        - vernierLocY
    botCenterPixRow   = 0        - vernierLocY + imageNumPixelRows

    # Build a map containing where segmentation is activated on the visual field, according to the eccentricity
    segMap = numpy.zeros((imageNumPixelRows, imageNumPixelCols))
    if useCMF:

        # Parameters in the transformed space
        eccInPixT = b * numpy.log(eccInPix + a)
        topLeftPixRowT      = numpy.imag(b * numpy.log( leftCenterPixCol + 1j*topCenterPixRow + a))
        botLeftPixRowT      = numpy.imag(b * numpy.log( leftCenterPixCol + 1j*botCenterPixRow + a))
        leftCenterPixColT   = numpy.real(b * numpy.log( leftCenterPixCol + 1j*0               + a))
        rightCenterPixColT  = numpy.real(b * numpy.log(rightCenterPixCol + 1j*0               + a))
        imageNumPixelsRowsT = int(    botLeftPixRowT - topLeftPixRowT)
        imageNumPixelsColsT = int(rightCenterPixColT - leftCenterPixColT)

        # Distorted map that contains a cortical representation of the visual space, plus where signals are active
        pad = max(10, int(5*ecc+(ecc%2!=0))) # even number used to draw segmentation signals that fall just by the boundaries of the image
        dstSegMap = numpy.zeros((imageNumPixelsRowsT+pad, imageNumPixelsColsT+pad))

        # Choose where the segmentation signal falls on the cortical space ONLY WORKS FOR HORIZONTAL FLANKING PATTERNS
        segLocYT  = numpy.imag(b * numpy.log(eccInPix +           1j*segLocY + a)) - topLeftPixRowT
        segLocXT  = numpy.real(b * numpy.log(eccInPix + segLocX + 1j*0       + a)) - leftCenterPixColT
        segLocXT += numpy.sign(segLocX)*segSize # correction with segSize so that I can change segSize without worrying about it
        segLocYT  = int(round(random.gauss(segLocYT, segSD)))
        segLocXT  = int(round(random.gauss(segLocXT, segSD)))
        if eccInPix + segLocX + a < 0:
            segLocXT = -numpy.inf # if the segmentation signal falls on the other side of the visual field

        # Select where the seg. signal will be ON, (circle around target on the cortical map, distorted on visual space)
        for r in range(-pad/2,imageNumPixelsRowsT+pad/2):
            for c in range(-pad/2,imageNumPixelsColsT+pad/2):

                # Compute distance between the cortical pixel and the center of the segmentation signal
                R = numpy.sqrt((c-segLocXT)**2 + (r-segLocYT)**2) - segSize
                A = 2 # smoothing borders of the segmentation signal
                if R < A:

                    # Compute visual space coordinates corresponding to the cortical coordinates
                    W = (c + leftCenterPixColT) + 1j*(r + topLeftPixRowT)
                    Z = numpy.exp(W/b) - a
                    rowFloat = numpy.imag(Z) - topCenterPixRow
                    colFloat = numpy.real(Z) - leftCenterPixCol

                    # Where exactly the distorted pixel falls on the original pixel map
                    row = int(rowFloat)
                    col = int(colFloat)
                    residualRow = rowFloat - row  # how close from the original row+1 this row result lies (0.1 = very far ; 0.9 = very close)
                    residualCol = colFloat - col  # how close from the original col+1 this col result lies (0.1 = very far ; 0.9 = very close)

                    # Build a 'kernel' to take care of aliasing effects, grows with distance to the fixation point
                    dist         = numpy.sqrt(((colFloat+leftCenterPixCol)**2 + (rowFloat+topCenterPixRow)**2)) / 30
                    kernelSize   = max(1, int(2*dist) + (1-(int(2*dist)%2))) # odd number growing with distance to fovea
                    kernelRadius = (kernelSize-1)/2
                    kernel       = numpy.zeros((kernelSize, kernelSize))
                    mu           = (kernelRadius+residualRow, kernelRadius+residualCol)
                    sigma        = 2*dist
                    for i in range(kernelSize):
                        for j in range(kernelSize):
                            kernel[i,j] = -((i-mu[0])**2+(j-mu[1])**2)
                    kernel = numpy.exp(kernel/(2*sigma**2))
                    kernel /= numpy.sum(kernel)

                    # Add a segmentation signal at the correct location
                    if row+kernelRadius >= 0 and col+kernelRadius >= 0:

                        # Pixels on the visual space that will receive a signal from the cortical pixel (+ kernel boundary issues)
                        rowRange = range(max(0,row-kernelRadius),min(imageNumPixelRows,row+kernelRadius+1))
                        colRange = range(max(0,col-kernelRadius),min(imageNumPixelCols,col+kernelRadius+1))
                        kernelRowRange = range(max(0,kernelRadius-row),min(kernelSize,imageNumPixelRows-row+kernelRadius))
                        kernelColRange = range(max(0,kernelRadius-col),min(kernelSize,imageNumPixelCols-col+kernelRadius))

                        # Add the signal to the concerned pixels on the visual space
                        if R > 0.0:
                            segMap[numpy.ix_(rowRange,colRange)] += ((A-R)/A)**2*kernel[numpy.ix_(kernelRowRange,kernelColRange)]
                        if R < 0.0:
                            segMap[numpy.ix_(rowRange,colRange)] += kernel[numpy.ix_(kernelRowRange,kernelColRange)]

        # Return the segmentation map
        if not numpy.sum(segMap) == 0.0:
            segMap = numpy.arctan(segMap/(0.3*numpy.max(segMap)))
            segMap /= numpy.max(segMap)
            segMap[segMap<0.3] = 0.0

    # Build a normal map given the segmentation signals
    else:

        # Compute the actual location of the segmentation signal centers
        sX = int(round(random.gauss(segLocX + numpy.sign(segLocX)*segSize + vernierLocX, segSD)))
        sY = int(round(random.gauss(segLocY + vernierLocY, segSD)))

        # Draw an undistorted map
        for i in xrange(imageNumPixelRows):
            for j in xrange(imageNumPixelCols):
                distance = numpy.sqrt((sX-j)**2 + (sY-i)**2)
                if distance < segSize:
                    segMap[i][j] = 1

    # Return the correct segmentation map
    return segMap


# Define illusory contours spreading delays, according to the general orientation of the stimulus
def generateDelays(thisConditionName, pixels, numOrientations):

    # Default delays
    HD1 = 0.0
    D1  = 0.0
    D1V = 0.0
    V   = 0.0
    VD2 = 0.0
    D2  = 0.0
    D2H = 0.0
    H   = 0.0

    # Custom delays
    if thisConditionName in ['text 1', 'text 2']:
        H = 10.0
    if thisConditionName in ['squares test', 'crosses', 'malania short 2', 'malania short 4', 'malania short 8', 'malania short 16', 'squares 2']:
        H = 20.0 # 30.0
    if thisConditionName in ['squares 3', 'octagons test', 'octagons grouping', 'circles 6', 'hexagons test', 'pattern stars 4', 'pattern stars 4b',
        'squares 4', 'circles 5', 'circles test', 'hexagons 3', 'octagons 3', 'stars 4', 'stars 10', 'stars 11', 'irreg1 4', 'irreg2 5']:
        H = 50.0 # 50.0
    if thisConditionName in ['VdB0', 'VdB1', 'VdB2']:
        H = 20.0
        V = 20.0

    # Generate the delays
    if numOrientations == 2:
        dampingDelays = [V, H]
    if numOrientations == 4:
        dampingDelays = [D1, V, D2, H]
    if numOrientations == 8:
        dampingDelays = [HD1, D1, D1V, V, VD2, D2, D2H, H]

    # Return the delays to the computer
    return dampingDelays
