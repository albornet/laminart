# Imports
import numpy, matplotlib.pyplot as plt

# Create different filters for orientation selectivity
def createConvFilters(nOri, size, phi, sigmaX, sigmaY, oLambda):

    # Initialize the filters
    filters = numpy.zeros((nOri, size, size))

    # Fill them with gabors
    midSize = (size-1.)/2.0
    maxValue = -1
    for k in range(0, nOri):
        theta = numpy.pi*(k+1)/nOri + phi
        for i in range(0, size):
            for j in range(0, size):
                x = (i-midSize)*numpy.cos(theta) + (j-midSize)*numpy.sin(theta)
                y = -(i-midSize)*numpy.sin(theta) + (j-midSize)*numpy.cos(theta)
                filters[k][i][j] = numpy.exp(-((x*x)/sigmaX + (y*y)/sigmaY)) * numpy.sin(2*numpy.pi*x/oLambda)

    # Normalize the orientation filters so that they have the same overall strength
    for k in range(nOri):
        filters[k] /= numpy.sqrt(numpy.sum(filters[k]**2))

    # Return the filters to the computer
    return (filters, -filters)

# Build connection pooling and connection filters arrays
def createPoolingConnectionsAndFilters(numOrientations, VPoolSize, phi):

    # Build the angles in radians, based on the taxi-cab distance (useful for 8 orientations)
    angles = []
    for k in range(numOrientations/2):
        taxiCabDistance = 4.0*(k+1)/numOrientations
        try:
            alpha = numpy.arctan(taxiCabDistance/(2 - taxiCabDistance))
        except ZeroDivisionError:
            alpha = numpy.pi/2 # because tan(pi/2) = inf
        angles.append(alpha + numpy.pi/4)
        angles.append(alpha - numpy.pi/4)

    # This is kind of a mess, but it works
    for k in range(len(angles)):
        if angles[k] <= 0.0:
            angles[k] += numpy.pi
        if numOrientations == 2: # special case ... but I could not do it otherwise
            angles[k] += numpy.pi/4

    # Sort the angles, because they are generated in a twisted way (hard to explain, but we can see that on skype)
    angles = numpy.sort(angles)

    # Set up orientation kernels for each filter
    midSize = (VPoolSize-1.0)/2.0
    VPoolingFilters = numpy.zeros((numOrientations, VPoolSize, VPoolSize))
    for k in range(0, numOrientations):
        theta = angles[k] + phi
        for i in range(0, VPoolSize):
            for j in range(0, VPoolSize):

                # Transformed coordinates: rotation by an angle of theta
                x =  (i-midSize)*numpy.cos(theta) + (j-midSize)*numpy.sin(theta)
                y = -(i-midSize)*numpy.sin(theta) + (j-midSize)*numpy.cos(theta)

                # If the rotated x value is zero, that means the pixel(i,j) is exactly at the right angle
                if numpy.abs(x) < 0.001:
                    VPoolingFilters[k][i][j] = 1.0

    # Set layer23 pooling connections (connect to points at either extreme of pooling line ; 1 = to the right ; 2 = to the left)
    VPoolingConnections1 = VPoolingFilters.copy()
    VPoolingConnections2 = VPoolingFilters.copy()

    # Do the pooling connections
    for k in range(numOrientations):

        # Want only the end points of each filter line (remove all interior points)
        for i in range(1, VPoolSize - 1):
            for j in range(1, VPoolSize - 1):
                VPoolingConnections1[k][i][j] = 0.0
                VPoolingConnections2[k][i][j] = 0.0

        # Segregates between right and left directions
        for i in range(VPoolSize):
            for j in range(VPoolSize):
                if j == (VPoolSize-1)/2:
                    VPoolingConnections1[k][0][j] = 0.0
                    VPoolingConnections2[k][VPoolSize-1][j] = 0.0
                elif j < (VPoolSize-1)/2:
                    VPoolingConnections1[k][i][j] = 0.0
                else:
                    VPoolingConnections2[k][i][j] = 0.0

    # for k in range(numOrientations):
    #     plt.figure()
    #     plt.imshow(VPoolingFilters[k], interpolation='nearest')
    #     plt.show()


    # Returns all the needed filters to the program
    return VPoolingFilters, VPoolingConnections1, VPoolingConnections2

# Set up filters for Brightness/Darkness/SurfaceSeg/BoundarySeg spreading and boundary blocking
def createBrightnessFilters(numOrientations, numBrightnessFlows, brightnessSpreadingSpeeds):

    # Set up filters for Brightness/Darkness filling-in stage (spreads in various directions) and boundary blocking
    brightnessFlowFilter = []
    H = numOrientations   - 1  # Vertical orientation index
    V = numOrientations/2 - 1  # Horizontal orientation index
    notStopFlowOrientation = [V, H]
    if numBrightnessFlows == 8:
        UpRight   = 1
        DownRight = 5
        notStopFlowOrientation = [V, H, UpRight, DownRight]

    brightnessBoundaryBlockFilter = []
    for k in range(len(brightnessSpreadingSpeeds)):
        if numBrightnessFlows == 4:      # Right, Down, Left, Up
            brightnessFlowFilter.append([[ brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                             brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                            -brightnessSpreadingSpeeds[k]]])
        if numBrightnessFlows == 8:      # Up, Right, Up, Left, UpRight, UpLeft, DownRight, DownLeft
            brightnessFlowFilter.append([[ brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                             brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                            -brightnessSpreadingSpeeds[k]],
                                         [ brightnessSpreadingSpeeds[k], -brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k], -brightnessSpreadingSpeeds[k]],
                                         [ brightnessSpreadingSpeeds[k],  brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k],  brightnessSpreadingSpeeds[k]]])
        brightnessBoundaryBlockFilter.append([])
        directionBBF1 = []
        directionBBF2 = []
        directionBBF3 = []
        directionBBF4 = []

        # One direction
        for d in range(1, (
            brightnessSpreadingSpeeds[k]+1)):  # First index indicates the only orientation that does NOT block flow
            directionBBF1.append([notStopFlowOrientation[0],  -d,      0   ])  # Up
            directionBBF1.append([notStopFlowOrientation[0],  -d,     -1   ])  # Up
            directionBBF2.append([notStopFlowOrientation[1],  -1,     -d   ])  # Right
            directionBBF2.append([notStopFlowOrientation[1],   0,     -d   ])  # Right
            directionBBF3.append([notStopFlowOrientation[0], ( d-1),   0   ])  # Up
            directionBBF3.append([notStopFlowOrientation[0], ( d-1),  -1   ])  # Up
            directionBBF4.append([notStopFlowOrientation[1],  -1,    ( d-1)])  # Left
            directionBBF4.append([notStopFlowOrientation[1],   0,    ( d-1)])  # Left
        brightnessBoundaryBlockFilter[k].append(directionBBF1)
        brightnessBoundaryBlockFilter[k].append(directionBBF2)
        brightnessBoundaryBlockFilter[k].append(directionBBF3)
        brightnessBoundaryBlockFilter[k].append(directionBBF4)

        if numBrightnessFlows == 8:  # Includes main diagonals
            directionBBF5 = []
            directionBBF6 = []
            directionBBF7 = []
            directionBBF8 = []
            for d in range(1,(brightnessSpreadingSpeeds[k]+1)):                    # First index indicates the only orientation that does NOT block flow
                directionBBF5.append([notStopFlowOrientation[2],  -d,     -d   ])  # UpRight
                directionBBF6.append([notStopFlowOrientation[3], ( d-1),  -d   ])  # UpLeft
                directionBBF7.append([notStopFlowOrientation[3],  -d,    ( d-1)])  # DownRight
                directionBBF8.append([notStopFlowOrientation[2], ( d-1), ( d-1)])  # DownLeft
            brightnessBoundaryBlockFilter[k].append(directionBBF5)
            brightnessBoundaryBlockFilter[k].append(directionBBF6)
            brightnessBoundaryBlockFilter[k].append(directionBBF7)
            brightnessBoundaryBlockFilter[k].append(directionBBF8)

    return brightnessFlowFilter, brightnessBoundaryBlockFilter


# Set up filters for boundary grouping spreads (illusory contours)
def createGroupingFilters(numOrientations, boundaryGroupingSpeeds, phi):

    # Set up filters for Boundary grouping (nearest neighbors); there are 2 filters, one for grouping in one direction and another for the other direction
    boundaryGroupFilter = [[] for s in range(2)]
    for side in range(2):                             # Grouping either or one side or on the other side
        sideValue = int((-2)*(side-0.5))              # Useful sign (1 for side = 0, -1 for side = 1)
        for h in range(len(boundaryGroupingSpeeds)):  # Grouping at different ranges

            # Identify center of filter
            center_xy = boundaryGroupingSpeeds[h]
            boundaryGroupFilter[side].append(numpy.zeros((numOrientations, numOrientations, 2*center_xy+1, 2*center_xy+1)))
            for k in range(numOrientations):          # Source orientation

                thetaSource = numpy.pi*(k+1)/numOrientations + phi
                if numOrientations == 8 and k in [0,2,4,6]:
                    threshold = 0.5
                else:
                    threshold = 1.0
                for k2 in [k]: # range(numOrientations):     # Target orientation

                    thetaTarget      = numpy.pi*(k2+1)/numOrientations + phi          # Originally numpy.pi*(k2)/numOrientations
                    orientationMatch = numpy.abs(numpy.cos(thetaSource-thetaTarget))  # Match of source and target orientations
                    for i in range(2*boundaryGroupingSpeeds[h]+1):
                        for j in range(2*boundaryGroupingSpeeds[h]+1):

                            thetaPixel = numpy.arctan2(-(i-center_xy), (j-center_xy))           # /!\ "-i" over "j" (trigonometry: "-y" over "x")
                            orientationMatch2 = numpy.abs( numpy.cos( thetaSource-thetaPixel))  # source orientation vs neighbor position
                            orientationMatch3 = numpy.abs( numpy.cos( thetaTarget-thetaPixel))  # target orientation vs neighbor position
                            orientationMatch4 = numpy.sin(-numpy.pi/2+thetaTarget-thetaPixel )  # target orientation vs neighbor position, to make directional filter
                            value = orientationMatch * orientationMatch2 * (orientationMatch3**3) * orientationMatch4

                            # Insert the value in the corresponding grouping filter
                            if sideValue*value >= threshold and not (i==center_xy and j==center_xy):    # put back 1.0!!!
                                distanceToCenter = numpy.sqrt((i-center_xy)**2 + (j-center_xy)**2)
                                boundaryGroupFilter[side][h][k][k2][i][j] = sideValue*value**3/distanceToCenter

                    # if k == k2:
                    #     print (thetaSource, thetaTarget)
                    #     for i in range(2*boundaryGroupingSpeeds[h]+1):
                    #         print ['%.1f' % boundaryGroupFilter[side][h][k][k2][i][j] for j in range(2*boundaryGroupingSpeeds[h]+1)]
                    #     print

    return boundaryGroupFilter


# Set up filters for segmentation spreading (use same architecture as grouping filters)
def createSegmentationFilters(numOrientations, boundarySegmentationSpeeds, phi):

    # The boundary segmentation flow filter is simply the smallest grouping flow filter, with no restriction for the side of flow and the target orientation
    boundarySegFlowFilters = []
    for s in boundarySegmentationSpeeds:
        basisFilters = createGroupingFilters(numOrientations, [s], phi)
        boundarySegFlowFilter = [basisFilters[0][0][ori][ori][:][:] + basisFilters[1][0][ori][ori][:][:] for ori in range(numOrientations)]
        for k in range(numOrientations):
            boundarySegFlowFilter[k][s][s] = 1.0  # 1.0 in the middle (why?)
        if numOrientations == 8 and s == 1:
            for k in range(numOrientations):
                if k in [0,2,4,6]:
                    boundarySegFlowFilter[k] = 0.5*(boundarySegFlowFilter[k-1] + boundarySegFlowFilter[k+1])
        boundarySegFlowFilters.append(boundarySegFlowFilter)

    # for k in range(numOrientations):
    #     plt.imshow(boundarySegFlowFilters[0][k])
    #     plt.show()

    return boundarySegFlowFilters
