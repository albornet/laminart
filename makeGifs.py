# Class used to generate gifs out of population's activities
import nest, numpy
from array2gif import write_gif
import matplotlib.pyplot as plt
import time

class gifMaker:

	def __init__(self, name, popID, dimTuple, orientedMap):

		self.name        = name                     # population name
		self.popID       = popID                    # actual population
		popSD            = nest.Create('spike_detector', numpy.prod(dimTuple))
		nest.Connect(popID, popSD, 'one_to_one')
		self.popSD       = popSD                    # population spike detector
		self.dimTuple    = dimTuple                 # population dimensions (rows, cols, etc.)
		self.plotVector  = numpy.zeros(len(popSD))  # used to plot each frame of the gif
		self.cumuVector  = numpy.zeros(len(popSD))  # used to clean the spike count from previous steps
		self.maxAllTime  = 1.0                      # used to normalize the images in the gif
		self.outImages   = [[] for i in range(dimTuple[0])]  # list of images that will build the gif
		self.orientedMap = orientedMap              # whether the gif takes colours into account
		self.takeMax     = True                     # whether only the maximally active orientation is plotted

	def takeScreenshot(self):

		# Record the spikes of the current step and update the cumulative vector (to substract previous spikes)
		self.plotVector  = nest.GetStatus(self.popSD, 'n_events') - self.cumuVector
		self.cumuVector += self.plotVector

		# Take care of output normalization
		maxThisTime     = numpy.max(self.plotVector)
		self.maxAllTime = max(maxThisTime, self.maxAllTime)

		# Either oriented output screenshot
		if self.orientedMap:

			(nSeg, nOri, nRow, nCol) = self.dimTuple
			thisPlot = numpy.reshape(self.plotVector, self.dimTuple)

			for h in range(nSeg):

				if self.takeMax:
					for i in range(nRow):
						for j in range(nCol):
							oriToSave = numpy.argmax(thisPlot[h,:,i,j])
							oriToKill = [x for x in range(nOri) if x != oriToSave]
							thisPlot[h,oriToKill,i,j] = 0.0

				if nOri == 2:
					self.outImages[h].append(numpy.dstack((thisPlot[h,0,:,:], thisPlot[h,1,:,:], numpy.zeros((nRow,nCol)))))
				if nOri == 4:
					self.outImages[h].append(numpy.dstack((thisPlot[h,1,:,:], thisPlot[h,3,:,:], numpy.maximum(thisPlot[h,0,:,:], thisPlot[h,2,:,:]))))
				if nOri == 8:
					rgbMap = numpy.array([[0.,.5,.5], [0.,0.,1.], [.5,0.,.5], [1.,0.,0.], [.5,0.,.5], [0.,0.,1.], [0.,.5,.5], [0.,1.,0.]])
					self.outImages[h].append(numpy.tensordot(thisPlot[h,:,:,:], rgbMap, axes=(0,0)))

		# Or simple output screenshot
		else:
			(nSeg, nRow, nCol) = self.dimTuple
			for h in range(nSeg):
				data = numpy.reshape(self.plotVector, self.dimTuple)[h,:,:]
				self.outImages[h].append(numpy.dstack((data, data, data)))

	def createGif(self, thisTrialDir, timeStepDuration, slowMoRate):

		# Create an animated gif of the output ; rescale firing rates to max value
		fps = 1000.0/(timeStepDuration*slowMoRate)
		if self.dimTuple[0] > 1:
			for h in range(self.dimTuple[0]):
				imagesInTheGif = 255.0*numpy.array(self.outImages[h])/self.maxAllTime
				gifPathName    = thisTrialDir+'/'+self.name+'Seg'+str(h)+'.gif'
				write_gif(       imagesInTheGif, gifPathName, fps=fps)
		else:
			imagesInTheGif     = 255.0*numpy.array(self.outImages[0])/self.maxAllTime
			gifPathName        = thisTrialDir+'/'+self.name+             '.gif'
			write_gif(           imagesInTheGif, gifPathName, fps=fps)

	def computeMatch(self, vernierTemplateRight, vernierTemplateLeft, vSide):

		templateScores = [[] for h in range(self.dimTuple[0])]
		for h in range(self.dimTuple[0]):
			for frame in self.outImages[h]:
				tempR = numpy.sum(frame[:,:,0]*vernierTemplateRight)
				tempL = numpy.sum(frame[:,:,0]*vernierTemplateLeft )
				templateScores[h].append(vSide*(tempR - tempL)/(100.0 + tempR + tempL))

		return numpy.transpose(templateScores)
