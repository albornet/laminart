# ! /usr/bin/env python
# NEST implementation of the LAMINART model of visual cortex.
import numpy as np, os, sys, random, nest
from scipy.signal  import convolve2d
from setInput      import *
from createFilters import *
from makeGifs import gifMaker
from multiprocessing import Pool, cpu_count


########################################
### All parameters of the simulation ###
########################################


# Main parameters (the following sections are secondary parameters)
ConditionNames      = ['octagons 3', 'squares 4', 'irreg1 4', 'circles 5', 'hexagons 3', 'irreg2 5', 'stars 4', 'stars 10']
# ConditionNames      = ['squares 1', 'circles 2', 'hexagons 2', 'octagons 2', 'stars 2', 'stars 6',  'irreg1 2', 'irreg2 2', 'malania short 1', 'malania short 2', 'boxes', 'crosses',
#                        'squares 4', 'circles 5', 'hexagons 3', 'octagons 3', 'stars 4', 'stars 10', 'irreg1 4', 'irreg2 5', 'malania short 4', 'malania short 8', 'boxes and crosses', 'pattern stars 4']
numTrials           =     1       # number of trials within each condition
resolution          =     0.1     # time step of the computation of the neurons dynamical equations
stimulusTime        =  2000.0     # stimulus duration [ms]

# General parameters
inputGain           =    1.0      # how much signal goes to LGN cells
inputScale          =    1.0      # general scale for all inputs to the network (LGN, grouping signal, segmentation signal)
templateSize        =   18        # size of the template (side length) ; in pixels ; 2*vBarLength-2 usually (18 for vBarLength = 10 pixels)
vSide               =    1        # side of the vernier (1 = bottom bar on the right ; -1 = bottom bar on the left) ; must correspond to actual stimulus
nCoresToUse         =    1        # define here how many cores you want to use (1 for normal for-loop)
nCoresToUse = min(max(   1,nCoresToUse),cpu_count()) # make sure that no irrelevant number is chosen

# How time goes
stepDuration        =   50.0      # duration of a time step, for the gif [ms]
interTrialInterval  =   10.0      # number of time steps between each stimulus ; originally 20.0
stimulusTimeSteps   =  stimulusTime/stepDuration # number of time steps of duration stepDuration
slowMoRate          =    4.0      # how much slower you see the neurons activity in the gifs ; originally 4.0

# Oriented filters parameters
nOri                =    8
convSize            =    6
phi                 =    0
sigmaX              =    0.5
sigmaY              =    convSize
oLambda             =    4.0
thresh              =  250
V1_SurroundRange    =    3
V2_SurroundRange    =    3

# Grouping mechanism parameters
dampingInput        =  100.0      # this input will block the boundary spreading control for a certain amount of time [pA]
longInput           =  400.0      # constant input controlling long range spreads
boundaryGroupSpeeds =   [1,5]     # [1,5] ; in pixel(s) per grouping step
groupTrigSegDelay   =  100.0      # time [ms] between the damping delay turn off and the segmentation signal turn on

# Segmentation parameters
nSegLayers          =    3        # number of segmentation layers ; each segmentation layer represents a group in the visual field
useSegmentation     =    1        # set to 0 if you want to have several segmentation layers, but not to send any segmentation signal
segSignalSize       =   10        # on the cortical map
segLocSD            =    4.1      # on the cortical map
segSignalAmplitude  =    1.0      # gain for the segmentation signals ; in pA (?)
segSignalDuration   =   50.0      # how long the segmentation signal lasts ; in ms
boundarySegSpeeds   =   [1]       # Could try with more... but segmentation is already fast ; in pixel(s) per spreading step

# Filling-in parameters
V4SpreadingRange    =    1                # V4 cells activity spontaneously spread
V1TriggerRange      =    int(convSize/2)  # V1 polar cells trigger activity in V4, on their polar receptive field size
numBrightnessFlows  =    4
brightSpreadSpeeds  =   [1]

# Cortical magnification parameters
useCMF              =    0        # use the cortical magnification factor to model the precision of segmentation signals
eccentricity        =    9.0      # distance (in degrees) between fixation point and Vernier
pixPerDeg           =  (20.0/1.4) # 20 pixels for the height of the Vernier (1.4 degrees)
a                   =    0.3*pixPerDeg # fovea "radius" ; 0.3 degrees (?), but in pixels
b                   =   60        # slope of the cortical magnification (could depend on pixPerDeg)

# Neurons parameters (only custom parameters of the neurons are defined here)
cellParams = {}                   # could be something like: "{'V_th': -50.0}"

# Connection parameters
connections = {

    # Input and LGN
    'InputToLGN'               :    700.0, #    700
    'LGN_ToV1Excite'           :   1500.0, #   1500

    # V1 classic
    'V1_ComplexExcite'         :   1500.0, #   1500
    'V1_SurroundInhib'         :   -200.0, #   -200 ; should depend on V1_SurroundRange
    'V1_CrossOriInhib'         :  -2000.0, #  -2000
    'V1_ToV2Excite'            :   5000.0, #   5000

    # V2 classic
    'V2_ComplexExcite'         :   2000.0, #   2000
    'V2_SurroundInhib'         :    -50.0, #    -50 ; should depend on V2_SurroundRange
    'V2_CrossOriInhib'         :  -2000.0, #  -2000
    'V2_SegmentExcite'         :   2000.0, #   2000
    'V2_SegmentInhib'          : -10000.0, # -10000

    # V2 grouping
    'V2_Spreading'             :    900.0, #    900 ; directionnal spreading, active until control inhibition kicks in
    'V2_SpreadingLong'         :    900.0, #    900 ; same, but at a greater range
    'V2_ControlExcite'         :   1000.0, #   1000 ; spreadingly activated interneurons activate spreading control
    'V2_ControlInhib'          :  -2000.0, #  -2000 ; how fast inhibition takes effect, once on
    'V2_InputLongExcite'       :     10.0, #     10 ; excitatory input to the inhibitory neurons that control the long-range spreading
    'V2_InputLongInhib'        :  -1000.0, #  -1000 ; inhibitory input to the same neurons, only active for a certain delay after simulus onset
    'V2_ControlLongExcite'     :   1000.0, #   1000 ; from V2Layer23 to the long-range spreading layer
    'V2_ControlLongInhib'      :  -3000.0, #  -3000 ; from the control layer to the long-range spreading layer
    'V2_ToV1FeedbackExcite'    :    300.0, #    300 ; from V2Layer23 to V1Layer4P1/2, to strengthen illusory contours

    # Boundary segmentation layers
    'B_SegmentSpreadExcite'    :    800.0, #    800
    'B_ControlSpreadExcite'    :   2000.0, #   2000
    'B_InterNeuronsInhib'      :  -2000.0, #  -2000
    'B_TriggerV2Inhib'         : -10000.0, # -10000
    'B_FeedbackV2Inhib'        : -10000.0, # -10000
    'B_SendSignalExcite'       :    500.0, #    500
    'B_ResetSignalInhib'       :  -1000.0, #  -1000

    # V4 filling-in with V2 and V1
    'V4_Competition'           :  -5000.0, #  -5000
    'V4_Spreading'             :   1000.0, #   1000
    'V2_TriggerSpreading'      :   1000.0, #   1000
    'V2_StopSpreading'         :   -400.0, #   -400
    'V4_SegExcite'             :   1000.0} #   1000

# Small correction if neuron dynamics time-step is made bigger to gain computation time
if resolution > 0.1:
    connections['V2_Spreading'       ] +=  65.0*(resolution-0.1)/(1.0-0.1)  # so that makes   965.0 for resolution = 1.0
    connections['V2_SpreadingLong'   ] +=  65.0*(resolution-0.1)/(1.0-0.1)  # so that makes   965.0 for resolution = 1.0
    connections['B_InterNeuronsInhib'] -= 300.0*(resolution-0.1)/(1.0-0.1)  # so that makes -2300.0 for resolution = 1.0

# Main loop, to run one condition
def runOneStim(thisConditionName):


    #########################################################################
    ### Read the input, build orientation filters and connection patterns ###
    #########################################################################


    # Read the image from Stimuli/ and create boundary coordinates (between pixels coordinates)
    pixels, imRows, imCols = readBMP(thisConditionName, onlyZerosAndOnes=2)
    oriRows = imRows + 1
    oriCols = imCols + 1
    oppOri  = list(numpy.roll(range(nOri), nOri/2))  # Indexes for orthogonal orientations
    V1ConvFilters1, V1ConvFilters2 = createConvFilters(    nOri, size=convSize,       phi=phi, sigmaX=sigmaX, sigmaY=sigmaY, oLambda=oLambda)
    boundaryGroupFilter            = createGroupingFilters(nOri, boundaryGroupSpeeds, phi=phi)
    dampingDelays                  = generateDelays(thisConditionName, pixels, nOri)  # Default is [0.0 in every direction]
    startSegSignal                 = max(dampingDelays) + groupTrigSegDelay           # Segmentation signal is triggerd by grouping turn-off
    sys.stdout.write('\n\n\n*** Condition "' + str(thisConditionName) + '" (' + str(imRows) + 'x' + str(imCols) + ' pixels). ***\n')


    # Goes across all trials for each condition
    for trialCount in range(numTrials):


        ###################################################################################################
        ### Create the neuron layers (LGN, V1, V2, V4, Boundary Segmentation Layers) + Input Generators ###
        ###################################################################################################


        # Rebuild network for each trial (this always makes computational time lower)
        nest.sli_run('M_WARNING setverbosity') # avoid writing too much NEST messages
        nest.ResetKernel()
        nest.SetKernelStatus({'resolution': resolution, 'local_num_threads': 4, 'print_time': True})

        # LGN
        if trialCount == 0: sys.stdout.write('\nBuilding network: \tLGN,')
        LGNBrightInput = nest.Create('dc_generator',  imRows*imCols)
        LGNDarkInput   = nest.Create('dc_generator',  imRows*imCols)
        LGNBright      = nest.Create('iaf_psc_alpha', imRows*imCols, cellParams)
        LGNDark        = nest.Create('iaf_psc_alpha', imRows*imCols, cellParams)

        # Area V1
        if trialCount == 0: sys.stdout.write(' V1,')
        V1Layer4P1 = nest.Create('iaf_psc_alpha', nOri*oriRows*oriCols, cellParams)
        V1Layer4P2 = nest.Create('iaf_psc_alpha', nOri*oriRows*oriCols, cellParams)
        V1Layer23  = nest.Create('iaf_psc_alpha', nOri*oriRows*oriCols, cellParams)

        # Area V2 classic
        if trialCount == 0: sys.stdout.write(' V2,')
        V2Layer4   = nest.Create('iaf_psc_alpha', nOri*oriRows*oriCols, cellParams)
        V2Layer23  = nest.Create('iaf_psc_alpha', nOri*oriRows*oriCols, cellParams)

        # Area V2 grouping
        V2Layer23Control       = nest.Create('iaf_psc_alpha',            nOri*oriRows*oriCols)
        V2Layer23Inhib         = nest.Create('iaf_psc_alpha',            nOri*oriRows*oriCols)
        V2Layer23Inter1        = nest.Create('iaf_psc_alpha',            nOri*oriRows*oriCols)
        V2Layer23Inter2        = nest.Create('iaf_psc_alpha',            nOri*oriRows*oriCols)
        V2Layer23Seg           = nest.Create('iaf_psc_alpha', nSegLayers*nOri*oriRows*oriCols)
        GroupDampingCell       = nest.Create('dc_generator',             nOri)
        if len(boundaryGroupSpeeds) > 1:
            ConstantInputLong  = nest.Create('dc_generator',             nOri)
            SpreadControlLong  = nest.Create('iaf_psc_alpha',            nOri)
            V2Layer23LongGroup = nest.Create('iaf_psc_alpha', (len(boundaryGroupSpeeds)-1)*nOri*oriRows*oriCols)

        # Boundary segmentation
        if nSegLayers > 1:
            if trialCount == 0: sys.stdout.write(' Segmentation,')
            BoundarySegNet     = nest.Create('iaf_psc_alpha', (nSegLayers-1)*oriRows*oriCols)
            BoundarySegInter3  = nest.Create('iaf_psc_alpha', (nSegLayers-1)*oriRows*oriCols)
            BoundarySegSignal  = nest.Create('dc_generator',  (nSegLayers-1)*oriRows*oriCols)

        # Area V4
        if trialCount == 0: sys.stdout.write(' V4')
        V4Bright    = nest.Create('iaf_psc_alpha',            imRows*imCols)
        V4Dark      = nest.Create('iaf_psc_alpha',            imRows*imCols)
        V4BrightSeg = nest.Create('iaf_psc_alpha', nSegLayers*imRows*imCols)
        V4DarkSeg   = nest.Create('iaf_psc_alpha', nSegLayers*imRows*imCols)

        # Populations to record
        popToRecord = {}
        popToRecord['V1_4P1'] = {'ID': V1Layer4P1,   'dimTuple': (1,          nOri, oriRows, oriCols), 'orientedMap': True }
        popToRecord['V1_4P2'] = {'ID': V1Layer4P2,   'dimTuple': (1,          nOri, oriRows, oriCols), 'orientedMap': True }
        popToRecord['V1_23' ] = {'ID': V1Layer23,    'dimTuple': (1,          nOri, oriRows, oriCols), 'orientedMap': True }
        popToRecord['V2_4'  ] = {'ID': V2Layer4,     'dimTuple': (1,          nOri, oriRows, oriCols), 'orientedMap': True }
        popToRecord['V2_23' ] = {'ID': V2Layer23,    'dimTuple': (1,          nOri, oriRows, oriCols), 'orientedMap': True }
        popToRecord['V2_'   ] = {'ID': V2Layer23Seg, 'dimTuple': (nSegLayers, nOri, oriRows, oriCols), 'orientedMap': True }
        popToRecord['V4B'   ] = {'ID': V4Bright,     'dimTuple': (1,                 imRows,  imCols), 'orientedMap': False}
        popToRecord['V4D'   ] = {'ID': V4Dark,       'dimTuple': (1,                 imRows,  imCols), 'orientedMap': False}
        popToRecord['V4B_'  ] = {'ID': V4BrightSeg,  'dimTuple': (nSegLayers,        imRows,  imCols), 'orientedMap': False}
        popToRecord['V4D_'  ] = {'ID': V4DarkSeg,    'dimTuple': (nSegLayers,        imRows,  imCols), 'orientedMap': False}
        if useSegmentation and nSegLayers > 1:
            popToRecord['SegNet'] = {'ID': BoundarySegNet,    'dimTuple': (nSegLayers-1, oriRows, oriCols), 'orientedMap': False}
            popToRecord['Inter3'] = {'ID': BoundarySegInter3, 'dimTuple': (nSegLayers-1, oriRows, oriCols), 'orientedMap': False}


        #######################################################################
        ###  Neurons layers are defined, now set up connexions between them ###
        #######################################################################


        ############ LGN cells ############
        if trialCount == 0: sys.stdout.write('\nSetting up connections: LGN,')

        nest.Connect(LGNBrightInput, LGNBright, 'one_to_one', {'weight': connections['InputToLGN']})
        nest.Connect(LGNDarkInput,   LGNDark,   'one_to_one', {'weight': connections['InputToLGN']})


        ############ Area V1 ############
        if trialCount == 0: sys.stdout.write(' V1,')

        for k in range(nOri):                              # Orientations
            for i2 in range(-convSize/2, convSize/2):      # Filter rows
                for j2 in range(-convSize/2, convSize/2):  # Filter columns
                    source  = []
                    target  = []
                    source2 = []
                    target2 = []
                    for i in range(convSize/2, oriRows-convSize/2):      # Rows
                        for j in range(convSize/2, oriCols-convSize/2):  # Columns
                            if i+i2 >=0 and i+i2<imRows and j+j2>=0 and j+j2<imCols:

                                # Connections from LGN to oriented polarized V1 cells, using convolutionnal filters (1st polarity)
                                if abs(V1ConvFilters1[k][i2+convSize/2][j2+convSize/2]) > 0.1:
                                    source.append(                    (i+i2)*imCols  + (j+j2))
                                    target.append(k*oriRows*oriCols +  i    *oriCols +  j    )

                                # Connections from LGN to oriented polarized V1 cells, using convolutionnal filters (2nd polarity)
                                if abs(V1ConvFilters2[k][i2+convSize/2][j2+convSize/2]) > 0.1:
                                    source2.append(                   (i+i2)*imCols  + (j+j2))
                                    target2.append(k*oriRows*oriCols + i    *oriCols +  j    )

                    # Define the weight for this filter position and orientation
                    polarWeight1 = connections['LGN_ToV1Excite']*V1ConvFilters1[k][i2+convSize/2][j2+convSize/2]
                    polarWeight2 = connections['LGN_ToV1Excite']*V1ConvFilters2[k][i2+convSize/2][j2+convSize/2]

                    # LGN -> Layer 4 (simple cells) connections (no connections at the edges, to avoid edge-effects)
                    nest.Connect([LGNBright[s] for s in source],  [V1Layer4P1[t] for t in target],  'one_to_one', {"weight": polarWeight1})
                    nest.Connect([LGNDark[s]   for s in source],  [V1Layer4P2[t] for t in target],  'one_to_one', {"weight": polarWeight1})
                    nest.Connect([LGNBright[s] for s in source2], [V1Layer4P2[t] for t in target2], 'one_to_one', {"weight": polarWeight2})
                    nest.Connect([LGNDark[s]   for s in source2], [V1Layer4P1[t] for t in target2], 'one_to_one', {"weight": polarWeight2})

        # Convergent excitatory connection from both polarities of V1Layer4 to V1Layer23
        nest.Connect(V1Layer4P1, V1Layer23,  'one_to_one', {"weight": connections['V1_ComplexExcite']})
        nest.Connect(V1Layer4P2, V1Layer23,  'one_to_one', {"weight": connections['V1_ComplexExcite']})

     
        ############ Area V2 ############
        if trialCount == 0: sys.stdout.write(' V2,')

        # V1 projection to V2 and further to layers 23
        nest.Connect(V1Layer23, V2Layer4,  'one_to_one', {'weight': connections['V1_ToV2Excite'   ]})
        nest.Connect(V2Layer4,  V2Layer23, 'one_to_one', {'weight': connections['V2_ComplexExcite']})

        # Projection from V2Layer23 to the segmentation cells
        source  = []
        source2 = []
        source3 = []
        target  = []
        target2 = []
        target3 = []
        for k in range(nOri):
            for i in range(oriRows):
                for j in range(oriCols):
                    for h in range(nSegLayers):
                        source.append(                         k*oriRows*oriCols + i*oriCols + j)
                        target.append(h*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

                        # Boundary inhibition between segmentation layers (upward, stronger)
                        for h2 in range(h, nSegLayers-1):
                            source2.append( h    *nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                            target2.append((h2+1)*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

                        # Boundary inhibition between segmentation layers (downward, weaker)
                        if h!=0:
                            source3.append(h*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                            target3.append(0*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

        # Projection from V2Layer23 to the segmentation cells and inhibition between segmentation layers
        nest.Connect([V2Layer23[s]    for s in source ], [V2Layer23Seg[t] for t in target ], 'one_to_one', {'weight':     connections['V2_SegmentExcite']})
        nest.Connect([V2Layer23Seg[s] for s in source2], [V2Layer23Seg[t] for t in target2], 'one_to_one', {'weight':     connections['V2_SegmentInhib' ]})
        nest.Connect([V2Layer23Seg[s] for s in source3], [V2Layer23Seg[t] for t in target3], 'one_to_one', {'weight': 0.5*connections['V2_SegmentInhib' ]})


        ############ V1/V2 competition stages ############
        VSurroundRanges = [V1_SurroundRange, V2_SurroundRange]
        VSurroundInhibs = [connections['V1_SurroundInhib'], connections['V2_SurroundInhib']]
        for k in range(nOri):                                     # Source orientation
            for k2 in range(nOri):                                # Target orientation

                colinOriWeight = 1.0/(1.0 + 8.0/nOri*min((k-       k2 )%nOri, (       k2 -k)%nOri))  # how colinear   k and k2 are
                orthoOriWeight = 1.0/(1.0 + 8.0/nOri*min((k-oppOri[k2])%nOri, (oppOri[k2]-k)%nOri))  # how orthogonal k and k2 are

                # Close-range inhibition between similar orientation
                if colinOriWeight**2 > 0.1:
                    for V1_or_V2 in [0,1]:
                        VRange = VSurroundRanges[V1_or_V2]
                        for i2 in range(-VRange, VRange+1):       # Filter rows
                            for j2 in range(-VRange, VRange+1):   # Filter columns
                                if i2!=0 or j2!=0:

                                    source  = []
                                    source2 = []
                                    target  = []
                                    target2 = []
                                    for i in range(oriRows):      # Rows
                                        for j in range(oriCols):  # Columns
                                            if 0 <= i+i2 < oriRows and 0 <= j+j2 < oriCols:

                                                source.append(k *oriRows*oriCols +  i    *oriCols +  j    )
                                                target.append(k2*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                                    surroundWeight = colinOriWeight*VSurroundInhibs[V1_or_V2]/np.sqrt(i2**2+j2**2)
                                    if V1_or_V2 == 0: 
                                        nest.Connect([V1Layer23[s] for s in source], [V2Layer4[t]     for t in target], 'one_to_one', {'weight': surroundWeight})
                                    if V1_or_V2 == 1:
                                        nest.Connect([V2Layer4[s]  for s in source], [V2Layer23[t]    for t in target], 'one_to_one', {'weight': surroundWeight})

                # Local inhibition between different orientations
                if orthoOriWeight**2 > 0.1:

                    source  = []
                    target  = []
                    for i in range(oriRows):      # Rows
                        for j in range(oriCols):  # Columns

                                source.append(k *oriRows*oriCols + i*oriCols + j)
                                target.append(k2*oriRows*oriCols + i*oriCols + j)

                    crossOriWeight = orthoOriWeight*connections['V1_CrossOriInhib']
                    nest.Connect([V2Layer4[s]  for s in source], [V2Layer4[t]  for t in target], 'one_to_one', {'weight': crossOriWeight})
                    nest.Connect([V2Layer23[s] for s in source], [V2Layer23[t] for t in target], 'one_to_one', {'weight': crossOriWeight})


        ############ Grouping mechanisms ############
        for side in range(2):                                    # Grouping either on one side or the other
            sideValue = int((-2)*(side-0.5))                     # Useful sign (1 for side = 0, -1 for side = 1)
            for h in range(len(boundaryGroupSpeeds)):            # Different ranges of spreading
                center = boundaryGroupSpeeds[h]                  # Center of the grouping filter
                for k in range(nOri):                            # Source orientations
                    for k2 in range(nOri):                       # Target orientations
                        for i2 in range(-center, center+1):      # Filter rows
                            for j2 in range(-center, center+1):  # Filter columns

                                # Grouping connections on each side ; the connection weights are taken from the grouping filters
                                groupingWeight     = connections['V2_Spreading'    ]*boundaryGroupFilter[side][h][k][k2][i2+center][j2+center]
                                groupingWeightLong = connections['V2_SpreadingLong']*boundaryGroupFilter[side][h][k][k2][i2+center][j2+center]
                                if groupingWeight > 0:

                                    source  = []
                                    target  = []
                                    source2 = []
                                    target2 = []
                                    for i in range(oriRows):      # Rows
                                        for j in range(oriCols):  # Columns
                                            if i+i2 >= 0 and i+i2 < oriRows and j+j2 >= 0 and j+j2 < oriCols:

                                                # Connections among V2Layer23 cells and bipole circuit
                                                if h==0:
                                                    target.append(k *oriRows*oriCols +  i    *oriCols +  j    )
                                                    source.append(k2*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                                                # Connection from long range grouping cells to V2 cells
                                                elif len(boundaryGroupSpeeds) > 1:
                                                    source2.append((h-1)*nOri*oriRows*oriCols + k2*oriRows*oriCols + (i+i2)*oriCols + (j+j2))
                                                    target2.append(                             k *oriRows*oriCols +  i    *oriCols +  j    )

                                    # V2Layer23 spreads to neighbors and excites or inhibits interneurons on each side
                                    nest.Connect([V2Layer23[s] for s in source], [V2Layer23[t]       for t in target], 'one_to_one', {'weight':  groupingWeight          })
                                    nest.Connect([V2Layer23[s] for s in source], [V2Layer23Inter1[t] for t in target], 'one_to_one', {'weight':  sideValue*groupingWeight})
                                    nest.Connect([V2Layer23[s] for s in source], [V2Layer23Inter2[t] for t in target], 'one_to_one', {'weight': -sideValue*groupingWeight})

                                    # Grouping at longer ranges
                                    if len(boundaryGroupSpeeds) > 1:
                                        nest.Connect([V2Layer23LongGroup[s] for s in source2], [V2Layer23[t] for t in target2], 'one_to_one', {'weight': groupingWeightLong})

        # Grouping interneurons 1 and 2 excite the control layer, in turn controling V2Layer23
        nest.Connect(V2Layer23Inter1,  V2Layer23Control,  'one_to_one', {'weight': connections['V2_ControlExcite']})
        nest.Connect(V2Layer23Inter2,  V2Layer23Control , 'one_to_one', {'weight': connections['V2_ControlExcite']})
        nest.Connect(V2Layer23Control, V2Layer23,         'one_to_one', {'weight': connections['V2_ControlInhib' ]})

        # Damping on control (orientation specific)
        source  = []
        target  = []
        source2 = []
        target2 = []
        source3 = []
        target3 = []
        for k in range(nOri):             # Orientations
            for i in range(oriRows):      # Rows
                for j in range(oriCols):  # Columns
                    source.append(k)
                    target.append(k*oriRows*oriCols + i*oriCols + j)
                    for h in range(len(boundaryGroupSpeeds)-1):
                        source2.append(                         k                                )
                        target2.append(h*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                        source3.append(                         k*oriRows*oriCols + i*oriCols + j)
                        target3.append(h*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

        # These connections are active during the damping delay (orientation specific)
        nest.Connect(    [GroupDampingCell[s]  for s in source ], [V2Layer23Control[t]   for t in target ], 'one_to_one', {'weight': connections['V2_ControlInhib'     ]})
        if len(boundaryGroupSpeeds) > 1:
            nest.Connect( ConstantInputLong,                       SpreadControlLong,                       'one_to_one', {'weight': connections['V2_InputLongExcite'  ]})
            nest.Connect( GroupDampingCell,                        SpreadControlLong,                       'one_to_one', {'weight': connections['V2_InputLongInhib'   ]})
            nest.Connect([SpreadControlLong[s] for s in source2], [V2Layer23LongGroup[t] for t in target2], 'one_to_one', {'weight': connections['V2_ControlLongInhib' ]})
            nest.Connect([V2Layer23[s]         for s in source3], [V2Layer23LongGroup[t] for t in target3], 'one_to_one', {'weight': connections['V2_ControlLongExcite']})

        # # V2 is fed back to V1 (espescially the illusory contours)
        nest.Connect(V2Layer23, V1Layer23, 'one_to_one', {'weight': connections['V2_ToV1FeedbackExcite']})


        ########### Boundary segmentation network ###################
        if nSegLayers > 1:
            if trialCount == 0: sys.stdout.write(' Segmentation,')

            source  = []
            source2 = []
            source3 = []
            source4 = []
            source5 = []
            target  = []
            target2 = []
            target3 = []
            target4 = []
            target5 = []
            for h in range(nSegLayers-1):                               # Segmentation layers (not including baseline layer)
                for i in range(oriRows):                                # Rows
                    for j in range(oriCols):                            # Columns
                        for sIndex, s in enumerate(boundarySegSpeeds):  # TEST

                            # All spreading interactions
                            for i2 in range(-(s+1), (s+1)+1):
                                for j2 in range(-(s+1), (s+1)+1):
                                    if (i2 != 0 or j2 != 0) and 0 <= i+i2 < oriRows and 0 <= j+j2 < oriCols:

                                        # Segmentation spread control (further than segmentation spread itself)
                                        source.append(h*oriRows*oriCols +  i    *oriCols +  j    )
                                        target.append(h*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                                        # Segmentation spreads by itself
                                        if abs(i2) <= s and abs(j2) <= s:
                                            source2.append(h*oriRows*oriCols +  i    *oriCols +  j    )
                                            target2.append(h*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                            # All non-spreading interactions
                            for k in range(nOri):

                                # Segmentation network triggers activity in its corresponding segmentation layer in V2
                                for h2 in range(h+1):
                                    source3.append(h   *                         oriRows*oriCols + i*oriCols + j)
                                    target3.append(h2  *nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

                                # V2 (corresponding segmentation layer) triggers activity in the segmentation network
                                source4.append(   (h+1)*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                                target4.append(    h   *                         oriRows*oriCols + i*oriCols + j)

                                # V1 trigger spreading in the segmentation network (orientation specific)
                                source5.append(                                k*oriRows*oriCols + i*oriCols + j)
                                target5.append(    h   *                         oriRows*oriCols + i*oriCols + j)

            # Input to the segmentation network from the segmentation signals, inhibition from spreading control interneurons
            nest.Connect( BoundarySegSignal,                     BoundarySegNet,                         'one_to_one', {'weight': connections['B_SendSignalExcite'   ]})
            nest.Connect( BoundarySegInter3,                     BoundarySegNet,                         'one_to_one', {'weight': connections['B_InterNeuronsInhib'  ]})

            # Spreading, spreading control, and triggering (segmentation network disinhibit contours representation in its own segmentation layer in V2)
            nest.Connect([BoundarySegNet[s]  for s in source2], [BoundarySegNet[t]    for t in target2], 'one_to_one', {'weight': connections['B_SegmentSpreadExcite']})
            nest.Connect([BoundarySegNet[s]  for s in source ], [BoundarySegInter3[t] for t in target ], 'one_to_one', {'weight': connections['B_ControlSpreadExcite']})
            nest.Connect([BoundarySegNet[s]  for s in source3], [V2Layer23Seg[t]      for t in target3], 'one_to_one', {'weight': connections['B_TriggerV2Inhib'     ]})

            # V2 segmentation layer is fed back to the spreading control to stabilize its activity ; also, V2Layer23 pre-inhibit the spreading control
            nest.Connect([V2Layer23Seg[s]    for s in source4], [BoundarySegInter3[t] for t in target4], 'one_to_one', {'weight': connections['B_FeedbackV2Inhib'    ]})
            nest.Connect([V2Layer23[s]       for s in source5], [BoundarySegInter3[t] for t in target5], 'one_to_one', {'weight': connections['B_FeedbackV2Inhib'    ]})


        ############## Area V4 ################
        if trialCount == 0: sys.stdout.write(' V4')

        source  = []
        source2 = []
        source4 = []
        source5 = []
        source6 = []
        target  = []
        target2 = []
        target4 = []
        target5 = []
        target6 = []
        for i in range(imRows):
            for j in range(imCols):

                # V4 signals spread in any direction
                for i2 in [-V4SpreadingRange, V4SpreadingRange]:       # [-V4SpreadingRange, 0, V4SpreadingRange]
                    for j2 in [-V4SpreadingRange, V4SpreadingRange]:   # [-V4SpreadingRange, 0, V4SpreadingRange]
                        if i+i2 >=0 and i+i2<imRows and j+j2>=0 and j+j2<imCols:
                            
                            # Spread in the normal V4 layer
                            source.append( i    *imCols +  j    )
                            target.append((i+i2)*imCols + (j+j2))

                # V2 cells inhibit filling-in and V1 cells trigger filling-in in specific directions
                for k in range(nOri):                                  # Orientations
                    filterSize = V1ConvFilters1[k].shape[0]            # Size of the filter to span
                    for i2 in range(-filterSize/2, filterSize/2):      # Filter rows
                        for j2 in range(-filterSize/2, filterSize/2):  # Filter columns
                            if i+i2 >=0 and i+i2<imRows and j+j2>=0 and j+j2<imCols:
 
                                # Connection from V2Layer23 to V4 cells, on both sides (here, V1convFilters2 could be used as well)
                                if abs(V1ConvFilters1[k][i2+filterSize/2][j2+filterSize/2]) > 0.1:

                                    # First from normal V2Layer23 to normal V4
                                    source2.append(                         k*oriRows*oriCols +  i    *oriCols +  j    )
                                    target2.append(                                             (i+i2)* imCols + (j+j2))                                              
  
                                # Connection from V1Layer4 (1st polarity) to V4 cells, in the orthogonal direction as the V1 orientation
                                if V1ConvFilters1[k][i2+filterSize/2][j2+filterSize/2] > 0.1:
                                    source4.append(k*oriRows*oriCols +  i    *oriCols +  j    )
                                    target4.append(                    (i+i2)* imCols + (j+j2))

                                # Connection from V1Layer4 (2nd polarity) to V4 cells, in the orthogonal direction as the V1 orientation
                                if V1ConvFilters2[k][i2+filterSize/2][j2+filterSize/2] > 0.1:
                                    source5.append(k*oriRows*oriCols +  i    *oriCols +  j    )
                                    target5.append(                    (i+i2)* imCols + (j+j2))

                # V4 signal projects to all the V4 segmentation layers
                for h in range(nSegLayers):
                    source6.append(                  i*imCols + j)
                    target6.append(h*imRows*imCols + i*imCols + j)

        # V4 activity spreads in any direction
        nest.Connect([V4Bright[s]     for s in source ], [V4Bright[t]    for t in target ], 'one_to_one', {'weight': connections['V4_Spreading']})
        nest.Connect([V4Dark[s]       for s in source ], [V4Dark[t]      for t in target ], 'one_to_one', {'weight': connections['V4_Spreading']})

        # V2 non-polar activity stops spreading, so that filling-in stops at the shapes boundaries
        nest.Connect([V2Layer23[s]    for s in source2], [V4Bright[t]    for t in target2], 'one_to_one', {'weight': connections['V2_StopSpreading']})
        nest.Connect([V2Layer23[s]    for s in source2], [V4Dark[t]      for t in target2], 'one_to_one', {'weight': connections['V2_StopSpreading']})

        # V2 polar activity triggers spreading directionnally, so that the shape brightness is on the correct side
        nest.Connect([V1Layer4P1[s]   for s in source4], [V4Bright[t]    for t in target4], 'one_to_one', {'weight': connections['V2_TriggerSpreading']})
        nest.Connect([V1Layer4P2[s]   for s in source5], [V4Bright[t]    for t in target5], 'one_to_one', {'weight': connections['V2_TriggerSpreading']})
        nest.Connect([V1Layer4P1[s]   for s in source5], [V4Dark[t]      for t in target5], 'one_to_one', {'weight': connections['V2_TriggerSpreading']})
        nest.Connect([V1Layer4P2[s]   for s in source4], [V4Dark[t]      for t in target4], 'one_to_one', {'weight': connections['V2_TriggerSpreading']})

        # V4 brightness and darkness signals compete between each other
        nest.Connect( V4Bright,                           V4Dark,                           'one_to_one', {'weight': connections['V4_Competition']})
        nest.Connect( V4Dark,                             V4Bright,                         'one_to_one', {'weight': connections['V4_Competition']})

        # V4 normal projects to V4 segmentation layers
        nest.Connect([V4Bright[s]     for s in source6], [V4BrightSeg[t] for t in target6], 'one_to_one', {'weight': connections['V4_SegExcite']})
        nest.Connect([V4Dark[s]       for s in source6], [V4DarkSeg[t]   for t in target6], 'one_to_one', {'weight': connections['V4_SegExcite']})



        ############################################################
        ### Network is defined, now set up stimulus and simulate ###
        ############################################################


        # Do the following only once per condition
        if trialCount == 0:

            # Print the size and the number of connections of the network
            sys.stdout.write('.\nOrientations:  ' + str(nOri)       + '  ====\  Num. neurons:   ' + str(nest.GetKernelStatus('network_size'   )) +
                             ' \nSegm. layers:  ' + str(nSegLayers) + '  ====/  Num. synapses:  ' + str(nest.GetKernelStatus('num_connections')) + '\n')

            # Compute the position of the Vernier target and create templates for template match
            vPosX, vPosY, vTemplateR, vTemplateL = findVernierAndCreateTemplates(pixels, thisConditionName, templateSize)
            trialScores = []  # Will contain the Vernier evidence, for each trial

        # Initialize population recorders for all chosen populations (V4 is placed first in the list, because it computes template match)
        gifMakerList = []
        for name, pop in popToRecord.iteritems():
            if name == 'V4B_':
                gifMakerList.insert(0, gifMaker(name=name, popID=pop['ID'], dimTuple=pop['dimTuple'], orientedMap=pop['orientedMap']))
            else:
                gifMakerList.append(   gifMaker(name=name, popID=pop['ID'], dimTuple=pop['dimTuple'], orientedMap=pop['orientedMap']))

        # Make the current stimulus directory
        thisConditionDir = 'SimFigures/'+thisConditionName
        if not os.path.exists(thisConditionDir):
            os.makedirs(thisConditionDir)
        thisTrialDir = thisConditionDir+'/Trial'+str(trialCount)
        if not os.path.exists(thisTrialDir):
            os.makedirs(thisTrialDir)

        # Create an image of the stimulus
        plotPixels = pixels/254.0
        fileName   = thisTrialDir+'/stimulus.png'
        # numpy.save(fileName, plotPixels)

        # Set the timing of the simulation and stimulation
        startTime = 0.0  # trialCount*(stimulusTimeSteps+interTrialInterval)*stepDuration
        stopTime  = startTime + stimulusTimeSteps*stepDuration

        # Feed both LGN layers with the stimulus array (each pixel is a DC current)
        nest.SetStatus(LGNBrightInput, [{'amplitude':inputScale*inputGain*light, 'start':startTime, 'stop':stopTime} for light in numpy.reshape(numpy.maximum(pixels/127.0-1.0, 0.0), (imRows*imCols,))])
        nest.SetStatus(LGNDarkInput  , [{'amplitude':inputScale*inputGain*dark,  'start':startTime, 'stop':stopTime} for dark  in numpy.reshape(numpy.maximum(1.0-pixels/127.0, 0.0), (imRows*imCols,))])

        # Damping constant input, to control the amount of grouping
        nest.SetStatus(GroupDampingCell,      [{'amplitude':inputScale*dampingInput, 'start': startTime, 'stop': startTime + dampingDelays[k]} for k in range(nOri)])
        if len(boundaryGroupSpeeds) > 1:
            nest.SetStatus(ConstantInputLong, [{'amplitude':inputScale*longInput,    'start': startTime, 'stop': stopTime                    } for k in range(nOri)])

        # Set the segmentation signals for each non-basic segmentation layer
        if nSegLayers > 1 and useSegmentation:
            segSignalPlot      = numpy.zeros((oriRows, oriCols))
            segSignalLocations = chooseSegmentationSignal(thisConditionName, nSegLayers)
            for h in range(nSegLayers-1):

                # Build the segmentation map, according to the input and whether CMF is used or not
                [segLocX, segLocY] = [segSignalLocations[2*h], segSignalLocations[2*h+1]]
                segMap = createSegMap(useCMF, pixels, thisConditionName, segLocX, segLocY, segSignalSize, segLocSD, eccentricity, pixPerDeg, a, b)
                segMap = numpy.hstack((segMap, numpy.zeros((segMap.shape[0], 1))))
                segMap = numpy.vstack((segMap, numpy.zeros((1, segMap.shape[1]))))
                segSignalPlot += segMap

                # Send the signal to the segmentation layer
                for i in range(oriRows):                                 # Rows
                    for j in range(oriCols):                             # Columns
                        A = inputScale*segSignalAmplitude*segMap[i,j]
                        nest.SetStatus([BoundarySegSignal[h*oriRows*oriCols + i*oriCols + j]], [{"amplitude": A, "start": startTime + startSegSignal, "stop": startTime + startSegSignal + segSignalDuration}])

        # Simulate the network
        sys.stdout.write('\nSimulating trial '+str(trialCount)+' for condition "'+str(thisConditionName) + '"')
        for time in range(int(stimulusTimeSteps)):

            # Run the simulation for one gif frame
            nest.Simulate(stepDuration)
            if time < stimulusTimeSteps-1:
                sys.stdout.write("\033[2F")  # move the cursor back to previous line

            # Take screenshots of every recorded population
            for instance in gifMakerList:
                instance.takeScreenshot()

        # Compute match with Vernier mask, for each frame (the first element of the list is the gif of V4Bright)
        templateMatches = gifMakerList[0].computeMatch(vTemplateR, vTemplateL, vSide)
        f = open(thisTrialDir+'/TemplateMatchLevels.txt', 'w')
        f.write('\n'+'Time'.rjust(6)+'\t'+str([('Seg'+str(h)).ljust(4) for h in range(nSegLayers)])+'\n\n')
        for t in range(int(stimulusTimeSteps)):
            f.write(str(t*stepDuration).rjust(6)+'\t'+str(['%.2f' % s for s in templateMatches[t]])+'\n')
        f.write('\n'+'Mean'.rjust(6)+'\t'+str(['%.2f' % s for s in numpy.mean(templateMatches, axis=0)])+'\n')
        f.close()
        trialScores.append(max(numpy.mean(templateMatches, axis=0)))

        # Create animated gif of stimulus
        sys.stdout.write('Creating animated gifs.\r')
        for instance in gifMakerList:
            instance.createGif(thisTrialDir, timeStepDuration=stepDuration, slowMoRate=slowMoRate)
        sys.stdout.write('                       '                )  # delete the gif message
        if not trialCount == numTrials: sys.stdout.write('\033[3F')  # move the cursor back to 3 lines above
    
    # Compute model evidence (and hence crowding) for the condition
    f = open(thisConditionDir+'/AverageModelEvidence.txt', 'w')
    f.write('\n'+'Trial'.rjust(8)+'   ModelEvidence\n')
    for trial in range(len(trialScores)):
        f.write(str(trialCount).rjust(8)+'   '+'%.2f' % trialScores[trial]+'\n')
    f.write('\n'+'Mean'.rjust(8)+' = '+str(numpy.mean(trialScores))+'\n')
    f.write('Thresh'.rjust(8)+' = '+str(0.5-numpy.mean(trialScores))+'\n')
    f.write('StdDev'.rjust(8)+' = '+str(numpy.std(trialScores))+'\n')
    f.close()
    sys.stdout.write('\n\n')


###############################################
### Main loop (using multithreading or not) ###
###############################################


if __name__ == '__main__':

    # Display welcome message
    sys.stdout.write('\n\n#########################################')
    sys.stdout.write(  '\n##                                     ##')
    sys.stdout.write(  '\n## -- WELCOME TO THE LAMINART MODEL -- ##')
    sys.stdout.write(  '\n##                                     ##')
    sys.stdout.write(  '\n#########################################')

    # Run the Laminart model for all conditions
    if nCoresToUse > 1:
        pool = Pool(nCoresToUse)              # Pool(n) to use n cores ; default is max number of cores
        pool.map(runOneStim, ConditionNames)  # Process the iterable using multiple cores
    else:
        for thisConditionName in ConditionNames:
            runOneStim(thisConditionName)

    # Display goodbye message
    sys.stdout.write('\n\n\n########################################')
    sys.stdout.write(    '\n##                                    ##')
    sys.stdout.write(    '\n## -- SIMULATION FINISHED. THANKS! -- ##')
    sys.stdout.write(    '\n##                                    ##')
    sys.stdout.write(    '\n########################################\n\n\n')
